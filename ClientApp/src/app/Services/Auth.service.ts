import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { OtpResponse } from './../models/otp-response';
import { AppConstant } from './../Constants/app.constants';
import { ValidateOtp } from '../models/validate-otp';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private Base_URL = AppConstant.Base_Url;

  constructor(private http: HttpClient) {}

  public generateOtp(userName: string): Observable<OtpResponse> {
    const params = new HttpParams().set('UserName', userName);
    return this.http.get<OtpResponse>(
      this.Base_URL + AppConstant.API.GenrateOTP,
      { params }
    );
  }

  public validateOtp(validateOtpModel: ValidateOtp): Observable<boolean> {
    return this.http.post<boolean>(
      this.Base_URL + AppConstant.API.ValidateOTP,
      validateOtpModel
    );
  }
}
