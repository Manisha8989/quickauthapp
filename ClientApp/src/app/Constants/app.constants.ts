import { environment } from './../../environments/environment.prod';

export class AppConstant {
  public static API = {
    GenrateOTP: "Auth/generate-otp",
    ValidateOTP: "Auth/validate-otp"
  }
 public static Base_Url = environment.BASE_URL;
}
