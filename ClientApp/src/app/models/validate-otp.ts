export class ValidateOtp {
  username: string;
  otp?: string | undefined;
  constructor(username: string, otp: string) {
    this.username = username;
    this.otp = otp;
  }
}
