import { Component } from '@angular/core';
import { interval, Subscription } from 'rxjs';

import { OtpResponse } from '../../models/otp-response';
import { ValidateOtp } from '../../models/validate-otp';
import { AuthService } from '../../Services/Auth.service';

@Component({
  selector: 'app-generate-otp',
  templateUrl: './generate-otp.component.html',
  styleUrls: ['./generate-otp.component.scss'],
})
export class GenerateOtpComponent {
  username: string = '';
  otp: string | undefined;
  enteredOtp: string = '';
  countdown: number = 30;
  validationMessage: string | undefined;
  private timerSubscription: Subscription | null = null;

  constructor(private authService: AuthService) {}

  generateOtp() {
    this.enteredOtp = '';
    if (!this.username) {
      return;
    }

    this.authService
      .generateOtp(this.username)
      .subscribe((response: OtpResponse) => {
        this.otp = response?.otp;
        this.countdown = 30;
        this.validationMessage = undefined;

        if (this.timerSubscription) {
          this.timerSubscription.unsubscribe();
        }

        this.timerSubscription = interval(1000).subscribe(() => {
          this.countdown--;
          if (this.countdown <= 0) {
            if (this.timerSubscription) {
              this.timerSubscription.unsubscribe();
            }
            this.otp = undefined;
            this.countdown = 30;
          }
        });
      });
  }

  validateOtp() {
    this.authService
      .validateOtp(new ValidateOtp(this.username, this.enteredOtp!))
      .subscribe((isValid: boolean) => {
        this.validationMessage = isValid
          ? 'OTP is valid!'
          : 'Invalid OTP. Please try again.';
      });
  }

  ngOnDestroy() {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
